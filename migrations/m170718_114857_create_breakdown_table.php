<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m170718_114857_create_breakdown_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('breakdown', [
            'id' => $this->primaryKey(),
			'title' => $this->string(),
			'levelId' => $this->integer(),
			'statusId' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('breakdown');
    }
}
